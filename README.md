# README #

## Quickstart - all I gotta do is download it and run one command :-J

* download, clone, whatever this repo
* change to the directory/folder with the scripts files (cd ./scripts)
* launch a container:
** docker run -it $(docker build -q -t thomasbedgar/bashit/scripts:v1 .)
* play around; run:
** scripts --help
** scripts -?
** s -i
** colors
** labels
** cat /etc/passwd | black-green -r bash
* Another words, poke around, and make other scripts.   I'll be updating with more functionality soon.


# tl;dr #

To use these bash scripts, referred to $SCRIPTS install into $SCRIPTS (you set to a directory of your choice)

* Download and deposit in your scripts bin directory ($SCRIPTS); change in bash.bashrc.profile as needed
* Dot the environment file bash.bashrc.local
* Start perusing the scripts (mks to create; pgs to display; es to edit; etc)

### Set up thse things ###

* Add these scripts to ($SCRIPTS) directory (E.g: ~/.local/bin, /usr/local/bin, ~/scripts/)
* Configuration
* Run each command with (-?) or (-help) 
* Set SCRIPTS environment variable in bash.bashrc.local and dot it (or . dot-funcs does the same thing)

### Contribution guidelines ###

* Use -? to get help on each command;  determine test cases from options
* Code review - use pgs to view scripts
* Let me know if there is a dependency missing.  The goal is to make functions and load at install time

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
