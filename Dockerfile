#
#  The build image will contain scripts from Tom Edgar
#
FROM docker.io/library/ubuntu:hirsute-20211107

ENV USER=scripts \
    TARGET_DIR=/usr/local/bin \
    DEBIAN_FRONTEND=noninteractive

WORKDIR $TARGET_DIR

RUN apt update && \
    apt install --no-install-recommends -y less && \
    ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime && \
    apt install -y tzdata && \
    dpkg-reconfigure --frontend noninteractive tzdata && \ 
    apt install --no-install-recommends -y vim && \
    useradd -s /bin/bash -m -d /home/$USER $USER && \
    mkdir /home/$USER/.notes

COPY . /usr/local/bin
COPY .notes /home/$USER/.notes

RUN chown -R $USER:$GROUP $TARGET_DIR /home/$USER/.notes && \
    rm -f /home/$USER/.bashrc && \
    rm -f /root/.bashrc && \
    ln -s $TARGET_DIR/bash.bashrc.local /home/$USER/.bashrc && \
    ln -s $TARGET_DIR/bash.bashrc.local /root/.bashrc


ENTRYPOINT ["/bin/bash"]
